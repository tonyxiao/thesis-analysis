run prepare_all

set more off
log using results/regressions, text replace

// Worldwide regression
regress tfp pcCountP100 inetUsersP100 i.cc

// Regression by Income Group
regress tfp pcCountP100 inetUsersP100 i.cc if incomegroup == "High income: OECD"
regress tfp pcCountP100 inetUsersP100 i.cc if incomegroup == "High income: nonOECD"
regress tfp pcCountP100 inetUsersP100 i.cc if incomegroup == "Low income"
regress tfp pcCountP100 inetUsersP100 i.cc if incomegroup == "Lower middle income"
regress tfp pcCountP100 inetUsersP100 i.cc if incomegroup == "Upper middle income"

// Regression by Region
regress tfp pcCountP100 inetUsersP100 i.cc if region == "East Asia & Pacific"
regress tfp pcCountP100 inetUsersP100 i.cc if region == "Europe & Central Asia"
regress tfp pcCountP100 inetUsersP100 i.cc if region == "Latin America & Caribbean"
regress tfp pcCountP100 inetUsersP100 i.cc if region == "Middle East & North Africa"
regress tfp pcCountP100 inetUsersP100 i.cc if region == "North America"
regress tfp pcCountP100 inetUsersP100 i.cc if region == "South Asia"
regress tfp pcCountP100 inetUsersP100 i.cc if region == "Sub-Saharan Africa"

log close
set more on
