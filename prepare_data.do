// Let's make sure all the proper directory structure exists
capture mkdir data/wb
capture mkdir results
capture mkdir results/graphs
capture mkdir tmp
capture mkdir tmp/wb
capture mkdir tmp/graphs

// Reload our ado programs from disk
discard
local wd: pwd
adopath ++ "`wd'/ado"

// ------- OECD Hours Data ------- //

capture confirm file "tmp/raw_hours.dta"

if _rc != 0 | "`1'" == "refresh" | "`2'" == "refresh" {
	// Process hours worked data from OECD
	capture rm tmp/raw_hours.dta
	insheet using data/oecd_hours_worked.csv, clear
	reshape long yr, i(country) j(year)
	rename yr hoursWorked
	label var hoursWorked "Hours Worked per Year"
	drop countryname
	save tmp/raw_hours, replace
}

// ------- Kiel OECD Capital Stock Data ------- //

capture confirm file "tmp/kiel_cap_stock.dta"

if _rc != 0 | "`1'" == "refresh" | "`2'" == "refresh" {
	insheet using data/kiel_oecd_capital_stock_percent_real_gdp.csv, clear
	reshape long yr, i(country) j(year)
	rename yr capStockP100gdp
	label var capStockP100gdp "Net Capital Stock as a % of Real GDP"
	drop countryname
	save tmp/kiel_cap_stock, replace
}



// ------- Base data (ITU + WB) ------- //

capture confirm file "tmp/base.dta"

if _rc != 0 | "`1'" == "refresh" | "`2'" == "refresh" {
	// Just in case something goes wrong we don't use stale files
	capture rm tmp/base.dta
	capture rm tmp/load_global_macros.do
	capture rm tmp/load_all_data.do

	set more off

	// Execute the python script
	if "`1'" == "refresh" {
		mac shift
	}
	shell python prepare_data.py `1'
	
	// Run the automatically generated do files
	do tmp/load_global_macros
	do tmp/load_all_data

	// Make sure all the variables exist	
	foreach v in $series {
		capture confirm variable `v'
		if _rc != 0 {
			di "var missing, generating dummy `v'"
			quietly generate `v' = .
		}
	}

	// Encode the country variable
	rename countrycode country
	encode country, gen(cc)
	label var cc "Country Code"
	label var country "Country Code"
	label var countryname "Country Name"
	label var region "Region"
	label var incomegroup "Income Group"
	label var year "Year"
	order cc, first

	// Let's rename default variables to something sensible

	// Demographics

	capture rename p_i61 pop // Population
	// h_i62 // Households

	//# Internet

	capture rename iu_i99h inetUsersP100 // Internet users (%)
	capture rename isp1i_i993 inetSubsP100 // Internet subscriptions per 100 inhabitants
	capture rename iibpiu_i994u inetIntBandwidthPinetUser // International Internet bandwidth per Internet user
	capture rename it_net_secr_p6 inetServersP1m // Secure Internet servers (per 1 million people)
	capture rename dis_i4213d inetDialupSubs // Dial-up Internet subscriptions

	capture rename fbsp1i_i992 inetBroadbandP100 // Fixed broadband subscriptions per 100 inhabitants

	//# Computer
	capture rename nopc_i422 pcCount // Number of Personal Computers

	//# Mobile
	capture rename mcsp1i_i911 mSubP100 // Mobile cellular subscriptions per 100 inhabitants
	capture rename mcswatdcabsp1i_i911_mb mDataSubP100 // Mobile cellular subscriptions with access to data communication at broadband speed per 100 inhabitants.
	capture rename pcomcnp_i271pop mCoverageP100// Percent coverage of mobile cellular network (population)
	capture rename rfmnu_i741_ mRev // Revenue from mobile networks (US$)

	//# Telephone
	capture rename ftlp1i_i91 tpCount // Fixed telephone lines per 100 inhabitants
	capture rename rfftsu_i71_ tpRev // Revenue from fixed telephone service (US$)

	//# TV
	capture rename notmts_i965c tvSubs // Number of terrestrial multi-channel TV subscribers
	capture rename nots_i965 tvCount // Number of TV sets

	//# Radio
	capture rename nors_i955 radioCount // Number of radio sets

	//# General Communications 

	// tfts_i51 // Total full-time telecommunication staff
	capture rename taiitu_i81_ teleInv // Total annual investment in telecommunication (US$)
	capture rename trfatsu_i75_ teleRev // Total revenue from all telecommunication services (US$)
	capture rename it_prt_news_p3 npPer1000 // Daily newspapers (per 1,000 people)

	//# General Technology

	capture rename ip_pat_nres patentsCountRes // Patent applications, nonresidents
	capture rename ip_pat_resd patentsCountNonRes // Patent applications, residents
	capture rename ip_tmk_totl trademarkCount // Trademark applications, total
	capture rename ip_jrn_artc_sc articlecount // Scientific and technical journal articles

	capture rename sp_pop_scie_rd_p6 researchersP1m // Researchers in R&D (per million people)
	capture rename sp_pop_tech_rd_p6 techniciansP1m // Technicians in R&D (per million people)

	capture rename gb_xpd_rsdv_gd_zs rndExpPgdp // Research and development expenditure (% of GDP)
	capture rename tx_val_tech_mf_zs htExportsP // High-technology exports (% of manufactured exports)

	//# Economy
	capture rename sl_tlf_prim_zs laborPrimaryEduP // Labor force with primary education (% of total)
	capture rename sl_tlf_seco_zs laborSecondEduP // Labor force with secondary education (% of total)
	capture rename sl_tlf_tert_zs laborHigherEduP // Labor force with tertiary education (% of total)
	//capture rename // sl_tlf_cact_zs // Labor participation rate, total (% of total population ages 15+)
	capture rename sl_tlf_totl_in laborCount // Labor force, total
	//capture rename // ny_gdp_mktp_cd // GDP (current US$)
	capture rename ny_gdp_mktp_kd gdpReal // GDP (constant 2000 US$)
	//capture rename // ny_gdp_pcap_cd // GDP per capita (current US$)
	capture rename ny_gdp_pcap_kd gdpPerCapitaReal // GDP per capita (constant 2000 US$)
	//capture rename // ne_gdi_totl_cd // Gross capital formation (current US$)
	capture rename ne_gdi_totl_kd gcfReal // Gross capital formation (constant 2000 US$)
	capture rename ne_gdi_ftot_kd gfcfReal // Gross fixed capital formation (constant 2000 US$)
	capture rename bn_klt_dinv_cd fdiNom // Foreign direct investment, net (BoP, current US$)
	capture rename sl_uem_totl_zs unempPlabor // Unemployment, total (% of total labor force)
	// Save this for cache
	save tmp/base, replace

	set more on
}

// ------- Auto load the base dataset into memory ------- //
use tmp/base, clear
run tmp/load_global_macros

