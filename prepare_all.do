
clear
run analyses/totalFactorProductivity

gen pcCountP100 = pcCount / pop * 100
label var pcCountP100 "Number of PC per 100 inhabitants"
gen tfp = tfp1
replace tfp = tfp2 if missing(tfp)
label var tfp "Total Factor Productivity"
