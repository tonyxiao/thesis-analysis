import csv, os, sys, time, re
from itertools import izip
from lib.xlsx import Workbook
from lib.ordered_dict import OrderedDict

def load_config(filename): 
	codes = map(lambda l: re.sub(r'\B#.*$', '', l).strip(), open(filename))
	return filter(lambda c: c, codes)

def load_worksheet(workbook, sheetname):	
	rows = workbook[sheetname].rows().iteritems()
	rows = map(lambda r: map(lambda c: c.value, r[1]), rows)
	headers = rows[0]
	return map(lambda r: dict(zip(headers, r)), rows[1:])

def process_itu(countries, series):
	"Process the raw ITU file into an executable load_itu_data.do file"

	# Index data by ITU codes
	countries = dict(map(lambda r: (r['ITU Country Code'], r), countries))
	series = dict(map(lambda r: (r['Series code'], r), series))

	# Open Relevant files
	rawFile = open('data/itu_all.csv', 'rb')
	outFile = open('tmp/itu_pivoted.csv', 'wb')
	stataFile = open('tmp/load_itu_data.do', 'wb')

	# Strip out the non-selected series
	rows = filter(lambda r: (r[0] in series and r[2] in countries) or r[0] == 'Series code', csv.reader(rawFile))
	years = rows[0][4:]
	cols = OrderedDict({'countrycode': ['countrycode'], 'year': ['year']})

	# Parse the data into columns
	for r in rows[1:]:
		code = r[0]
		if code not in cols:
			cols[code] = [series[code]['Variable']]
		cols['countrycode'].extend([countries[r[2]]['Variable']]*len(years))
		cols['year'].extend(years)
		cols[code].extend(r[4:])

	# Write the processed csv out
	csv.writer(outFile).writerows(izip(*cols.values()))

	# Create Stata file to add labels to variables
	# TODO: Some variable names get longer than 80 characters, write the stata cmd to handle it properly
	stataFile.write('capture rm tmp/itu.dta\n')
	stataFile.write('insheet using tmp/itu_pivoted.csv, clear\n')
	for s in series.values():
		stataFile.write('label variable %s "%s"\n' % (s['Variable'], s['Series name']))
	stataFile.write('save tmp/itu, replace\n')

	# Clean up
	rawFile.close()
	outFile.close()
	stataFile.close()


def process_wb(countries, series):
	"Generate load_wb_data.do file"

	countryCodes = map(lambda c: c['Variable'], countries)
	seriesCodes = map(lambda c: c['Variable'], series)

	# Open relevant files
	stataFile = open('tmp/load_wb_data.do', 'wb')

	# Make sure first the datafiles from worldbank exists
	stataFile.write(
	'''
	clear
	set memory 1g
	capture rm tmp/wb.dta
	''')

	for country in countries:
		stataFile.write('local %s_name `"%s"\'\n' % (country['Variable'], country['Country Name']))

	stataFile.write(
	'''
	foreach country in %s {
		// Let's get the raw files
		capture confirm file "data/wb/wb_raw_`country'.dta"
		if _rc == 0 {
			display "data/wb/wb_raw_`country'.dta found, skip loading"
		}
		else {
			display "Loading data for `country'"
			wbopendata, country(`country') long clear
			save data/wb/wb_raw_`country', replace
		}
	}
	''' % ' '.join(countryCodes))

	# If there are no series selected, we are done
	if len(series) == 0:
		stataFile.close()
		return


	# Combine a worldbank dataset to only contain the selected countries and indicators
	stataFile.write(
	'''
	clear
	foreach country in %s {
		di "Appending data/wb/wb_raw_`country'.dta"
		append using data/wb/wb_raw_`country'
		local vars ""
		foreach series in %s {
			capture sum `series'
			if _rc == 0 {
				local vars "`vars' `series'"
			}
		}
		keep year countrycode `vars'
	}
	''' % (' '.join(countryCodes), ' '.join(seriesCodes)))

	stataFile.write('save tmp/wb, replace\n')

	# Clean up
	stataFile.close()

def process_all(countries, ituSeries, wbSeries):
	"Generate a single file for users to load"

	stataFile = open('tmp/load_all_data.do', 'wb')

	if len(countries)==0 or (len(wbSeries)==0 and len(ituSeries)==0):
		stataFile.write('clear\n')
		stataFile.write('di as error "No variables selected for loading" \n')
		stataFile.close()
		return

	stataFile.write('do tmp/load_itu_data\n')
	stataFile.write('do tmp/load_wb_data\n')
	
	if len(wbSeries) and len(ituSeries):
		stataFile.write(
		'''
		use tmp/itu, clear
		merge 1:1 year countrycode using tmp/wb
		drop _merge
		''')
	elif len(ituSeries):
		stataFile.write('use tmp/itu, clear\n')
	elif len(wbSeries):
		stataFile.write('use tmp/wb, clear\n')

	stataFile.write(
	'''
	gen countryname = ""
	gen region = ""
	gen incomegroup = ""
	''')
	for c in countries:
		code = c['Variable']
		stataFile.write(
		'''
		replace countryname = "%s" if countrycode == "%s"
		replace region = "%s" if countrycode == "%s"
		replace incomegroup = "%s" if countrycode == "%s"
		''' % (c['Country Name'], code, c['Region'], code, c['Income Group'], code))


	stataFile.write(
	'''
	sort countrycode year
	order countrycode countryname region incomegroup, first
	
	// Save the data file
	save tmp/data, replace
	''')

	stataFile.close()


def process_macros(countries, ituSeries, wbSeries):
	"Generate macros to be used in Stata later"

	# Open File
	stataFile = open('tmp/load_global_macros.do', 'wb')

	countryCodes = map(lambda s: s['Variable'], countries)
	series = map(lambda s: s['Variable'], wbSeries)
	series.extend(map(lambda s: s['Variable'], ituSeries))

	stataFile.write(
	'''
	global countryCount %s
	global countries %s
	global series %s
	''' %  (len(countryCodes), ' '.join(countryCodes), ' '.join(series)))

	# Clean up
	stataFile.close()


def main():
	# Load the series catalog from excel file
	workbook = Workbook('data/catalog.xlsx')

	countries = load_worksheet(workbook, '_Countries')
	wbSeries = load_worksheet(workbook, '_WB')
	ituSeries = load_worksheet(workbook, '_ITU')

	# Filter out the unselected ones
	if len(sys.argv) > 1:
		variables = set()
		for filename in sys.argv[1:]:
			variables.update(set(load_config(filename)))
		wbSeries = filter(lambda r: r['Variable'] in variables, wbSeries)
		ituSeries = filter(lambda r: r['Variable'] in variables, ituSeries)
	else:
		wbSeries = filter(lambda r: r['Include'] == '1', wbSeries)
		ituSeries = filter(lambda r: r['Include'] == '1', ituSeries)

	countries = filter(lambda r: r['Include'] == '1', countries)
	
	# Let users know what data will be prepared

	print '# --- Executing prepare_data.py ---'
	print '\n# Preparing all available years 1960 - 2011'
	print '\n# Selected Countries:'
	for c in countries:
		print '\t%s # %s' % (c['Variable'], c['Country Name'])
	print '\n# Selected ITU Series:'
	for s in ituSeries:
		print '\t%s # %s' % (s['Variable'], s['Series name'])
	print '\n# Selected WorldBank Indicators:'
	for s in wbSeries:
		print '\t%s # %s' % (s['Variable'], s['Series Name'])

	# Process data and output stata files
	process_itu(countries, ituSeries)
	process_wb(countries, wbSeries)
	process_all(countries, ituSeries, wbSeries)
	process_macros(countries, ituSeries, wbSeries)

	print '\n# --- Finished Executing prepare_data.py ---'


if __name__ == '__main__':
	main()
