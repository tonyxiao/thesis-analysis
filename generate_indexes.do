// Prepare data as usual
run prepare_data

// draw some graphs, ! have to pass arguments in using global macro
/*
global graphArgs ylabel(,format(%6.0g))
do prepare_graphs usa*
global graphArgs
*/

//dydx usa_iup_i99h year, g(usa_iup_i99h_dydx)
//twoway line usa_iup_i99h* year
