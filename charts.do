run prepare_all


// Capital stock charts
twoway (scatter capStockP100gdp year)  (lfitci capStockP100gdp year)
graph export results/timeInvarianceOfncpPgdp.png, replace


// Hours worked charts
xtline hoursWorked if year > 1999 & incomegroup == "High income: OECD", overlay legend(col(4))
graph export results/hours.png, replace

// Worldwide charts
/*xtline tfp if year >= 1980, overlay legend(off)  title("Worldwide Total Factor Productivity")
graph export results/tfp.png, replace
xtline inetUsersP100 if year >= 1980, overlay legend(off)  title("Adoption of Internet worldwide") aspectratio(0.3)
graph export results/inetUsers.png, replace
xtline pcCountP100 if year >= 1980, overlay legend(off) title("Adoption of PCs worldwide") aspectratio(0.3)
graph export results/pcCount.png, replace
*/
