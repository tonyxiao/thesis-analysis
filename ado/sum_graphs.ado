program define sum_graphs
	
	syntax varlist [, Cache(string)]
	set more off
	local tmp "tmp/graphs/"
	local dest "results/graphs/"
	shell rm `dest'*
	di "Using global graphArgs: ${graphArgs}"
	foreach v of varlist `varlist' {
		local label: var label `v'
		di "Processing " %25s "`v'" " - `label'"
		capture confirm file "`tmp'`v'.png"
		if _rc != 0 | `"`cache'"' != "" {
			graph twoway line `v' year, title(`"`label'"') ytitle("") ${graphArgs}
			quietly graph export "`tmp'`v'.png", replace
		}
		quietly copy "`tmp'`v'.png" "`dest'`v'.png", replace

	}
	set more on

end
