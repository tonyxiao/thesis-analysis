program define varlabelsfix
	confirm variable `1'
	local label: var label `1'
	local l_len = strlen(`"`label'"')
	local s_len = strlen(`"`2'"')
	if `l_len'+`s_len' > 80 {
		local label = substr(`"`label'"', 1, 77-`s_len') + "..."
	}
	label variable `1' `"`label'`2'"'
end
