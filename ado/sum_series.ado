program define sum_series
	syntax varlist
	
	tempname nObs
	quietly desc, short
	scalar `nObs' = r(N)

	di "Total # of observation is: " `nObs'

	di "  Bad   Obs. Count  Fill " %-20s "  Series Suffix" "     Series Name "
	tempname badCount
	foreach v of varlist `varlist' {
		local `badCount' = 0
		tempvar _count
		
		quietly codebook `v', problems
		if strlen("`r(cons)'") > 0 {
			local `badCount' = 1
		}
		egen `_count' = rownonmiss(`v'), strok
		local vl: var label `v'
		sum `_count', meanonly

		local bad `"%6s "   ``badCount''   ""'
		di  `bad' %7.0f r(sum) "/" `nObs' " " /// Bad count and obs. count
			%5.1f 100*r(sum)/`nObs' "%  " /// Fill %
			%-20s "`v'" " - " "`vl'" // Series suffix and name

		drop `_count'
	}

end
