run prepare_data analyses/used_series.txt // refresh

merge 1:1 country year using tmp/raw_hours
label var year "Year"
// Temporarily generate new panel for regression
encode country, gen(_cc) // Temporary Country Code
xtset _cc year

// regress the hours worked for each country using entity FE panel model
// Yit = a0 + a1X1,it + a2X2,it + akXk,it... + b1E1 + b2E2 + bnEn + uit
 reg hoursWorked year i._cc

// Predict using a linear model
predict hoursWorkedHat, xb

// Use the original data whenever possible
replace hoursWorkedHat = hoursWorked if !missing(hoursWorked)
label var hoursWorkedHat "`: var label hoursWorked' (Predicted)"

// Get rid of the temporary panel
drop if missing(cc)
xtset, clear
keep country year hoursWorked*

save tmp/hours, replace
