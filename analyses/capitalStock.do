run prepare_data analyses/used_series.txt // refresh

merge 1:1 country year using tmp/kiel_cap_stock

// Temporarily generate new panel for regression
encode country, gen(_cc) // Temporary Country Code
xtset _cc year

// Official Estimates made by Kiel Institute
gen capStockReal = gdpReal * capStockP100gdp / 100
label var capStockReal "Net Capital Stock (Real 2000$)"

// 1st Estimation Method - Geometric Depreciation at 4% (Perpetual Inventory Method)
gen capStockRealHat1 = capStockReal
replace capStockRealHat1 = 0.96*capStockRealHat1[_n-1] + gfcfReal if !missing(capStockRealHat1[_n-1]) & missing(capStockRealHat1)
label var capStockRealHat1 "Net Capital Stock (Real 2000$) estimated using PIM"

// 2nd Estimation Method - % of GDP

// Show that capital stock as % of GDP is incredibly consistent across time
* twoway lfitci capStockP100gdp year || scatter capStockP100gdp year, msize(tiny)

// Fixed Effect Panel Data linear regression
reg capStockP100gdp year i._cc
predict capStockP100gdpHat
gen capStockRealHat2 = gdpReal * capStockP100gdpHat / 100
label var capStockP100gdpHat  "`: var label capStockP100gdp' (Predicted)"
label var capStockRealHat2 "Net Capital Stock (Real 2000$) estimated using predicted Cap. Stock as % of GDP"

// Get rid of the temporary panel
xtset, clear
drop if missing(cc)
keep country year capStock*

save tmp/capStock, replace
