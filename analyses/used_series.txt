# ------------ Series ------------ #

# Demographics

p_i61 # Population
# h_i62 # Households

## Internet

iu_i99h # Internet users (%)
# isp1i_i993 # Internet subscriptions per 100 inhabitants
# iibpiu_i994u # International Internet bandwidth per Internet user
# it_net_secr_p6 # Secure Internet servers (per 1 million people)
# dis_i4213d # Dial-up Internet subscriptions

# fbsp1i_i992 # Fixed broadband subscriptions per 100 inhabitants

## Computer
nopc_i422 # Number of Personal Computers

## Mobile
# mcsp1i_i911 # Mobile cellular subscriptions per 100 inhabitants
# mcswatdcabsp1i_i911_mb # Mobile cellular subscriptions with access to data communication at broadband speed per 100 inhabitants.
# pcomcnp_i271pop # Percent coverage of mobile cellular network (population)
# rfmnu_i741_ # Revenue from mobile networks (US$)

## Telephone
# ftlp1i_i91 # Fixed telephone lines per 100 inhabitants

# rfftsu_i71_ # Revenue from fixed telephone service (US$)

## TV
# notmts_i965c # Number of terrestrial multi-channel TV subscribers
# nots_i965 # Number of TV sets

## Radio
# nors_i955 # Number of radio sets

## General Communications 

# tfts_i51 # Total full-time telecommunication staff
# taiitu_i81_ # Total annual investment in telecommunication (US$)
# trfatsu_i75_ # Total revenue from all telecommunication services (US$)
# it_prt_news_p3 # Daily newspapers (per 1,000 people)

## General Technology

# ip_pat_nres # Patent applications, nonresidents
# ip_pat_resd # Patent applications, residents
# ip_tmk_totl # Trademark applications, total
# ip_jrn_artc_sc # Scientific and technical journal articles

# sp_pop_scie_rd_p6 # Researchers in R&D (per million people)
# sp_pop_tech_rd_p6 # Technicians in R&D (per million people)

# gb_xpd_rsdv_gd_zs # Research and development expenditure (% of GDP)
# # bx_gsr_ccis_zs # ICT service exports (% of service exports, BoP)
# # tx_val_ictg_zs_un # ICT goods exports (% of total goods exports)
# tx_val_tech_mf_zs # High-technology exports (% of manufactured exports)

## Economy
sl_tlf_prim_zs # Labor force with primary education (% of total)
sl_tlf_seco_zs # Labor force with secondary education (% of total)
sl_tlf_tert_zs # Labor force with tertiary education (% of total)
# sl_tlf_cact_zs # Labor participation rate, total (% of total population ages 15+)
sl_tlf_totl_in # Labor force, total
# ny_gdp_mktp_cd # GDP (current US$)
ny_gdp_mktp_kd # GDP (constant 2000 US$)
# ny_gdp_pcap_cd # GDP per capita (current US$)
ny_gdp_pcap_kd # GDP per capita (constant 2000 US$)
# ne_gdi_totl_cd # Gross capital formation (current US$)
# ne_gdi_totl_kd # Gross capital formation (constant 2000 US$)
ne_gdi_ftot_kd # Gross fixed capital formation (constant 2000 US$)
bn_klt_dinv_cd # Foreign direct investment, net (BoP, current US$)
sl_uem_totl_zs # Unemployment, total (% of total labor force)


