run prepare_data analyses/used_series.txt // refresh
run analyses/hoursWorked
run analyses/capitalStock

use tmp/base, clear
merge 1:1 country year using tmp/hours
drop _merge
merge 1:1 country year using tmp/capStock
drop _merge

xtset cc year

gen inputHours = laborCount * (1-unempPlabor/100) * hoursWorkedHat
label var inputHours "Labor Hours per year"
gen tfp1 = gdpReal / (capStockRealHat1^0.36 * (inputHours)^0.64)
gen tfp2 = gdpReal / (capStockRealHat2^0.36 * (inputHours)^0.64)
label var tfp1 "Solow Residual (TFP) estimated using PIM Cap. Stock"
label var tfp2 "Solow Residual (TFP) estimated using % GDP Cap. Stock"
