
countries = open('data/lists/all_countries.txt', 'r').readlines()
series = open('data/lists/all_series.txt', 'r').readlines()

n = 1
out = open('analyses/data_quality/batch%d.txt' % n, 'w')

for i in range(len(series)):
	out.write(series[i])
	if i % 9 == 8:
		out.writelines(countries)
		out.close()
		print "Wrote to batch %s" % out.name
		n += 1
		out = open('analyses/data_quality/batch%d.txt' % n, 'w')

out.writelines(countries)
out.close()
