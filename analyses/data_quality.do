

// All do files should be run from the top level directory in the repo

/**
 You can take the results of the log file, first get rid of all the irrelevant 
 lines, then you'll also probably want to handle - (dashes) in some intelligent
 way because excel is silly enough to treat that as a separator
 Then run it through the following
 using sublime, then you'll get the file ready to be copy pasted into excel
 Find:    ^\s*(\S*)\s*(\S*)\s*(\S*)\s*(\S*)\s*- (.*)$
 Replace: $1\t$2\t$3\t$4\t$5

 */

shell python analyses/data_quality/make_batches.py

set more off
log using analyses/data_quality, text append
forvalues i = 1/159 {
	local f "analyses/data_quality/batch`i'.txt"
	confirm file "`f'"
	di "Analyze using `f'"
	run prepare_data `f' refresh
	sum_series
}
log close
set more on
